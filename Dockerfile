FROM registry.lendfoundry.com/base:beta8

ADD ./src/CapitalAlliance.Applications.Filters.Abstractions /app/CapitalAlliance.Applications.Filters.Abstractions
WORKDIR /app/CapitalAlliance.Applications.Filters.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/CapitalAlliance.Applications.Filters.Persistence /app/CapitalAlliance.Applications.Filters.Persistence
WORKDIR /app/CapitalAlliance.Applications.Filters.Persistence
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/CapitalAlliance.Applications.Filters /app/CapitalAlliance.Applications.Filters
WORKDIR /app/CapitalAlliance.Applications.Filters
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/CapitalAlliance.Applications.Filters.Client /app/CapitalAlliance.Applications.Filters.Client
WORKDIR /app/CapitalAlliance.Applications.Filters.Client
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/CapitalAlliance.Applications.Filters.Api /app/CapitalAlliance.Applications.Filters.Api
WORKDIR /app/CapitalAlliance.Applications.Filters.Api
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel