﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CreditExchange.StatusManagement.Client;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;

namespace CapitalAlliance.Applications.Filters
{
    public class EventsForTaggingListener : IEventsForTaggingListener
    {
        public EventsForTaggingListener(
            IConfigurationServiceFactory<TaggingConfiguration> apiConfigurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IFilterViewRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IApplicationFilterServiceFactory applicationFilterServiceFactory
            )
        {
            EventHubFactory = eventHubFactory;
            ApiConfigurationFactory = apiConfigurationFactory;
            TokenHandler = tokenHandler;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ApplicationFilterServiceFactory = applicationFilterServiceFactory;
        }

        private IStatusManagementServiceFactory StatusManagementFactory { get; }

        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }

        private IApplicantServiceClientFactory ApplicantServiceFactory { get; }

        private IConfigurationServiceFactory<TaggingConfiguration> ApiConfigurationFactory { get; }

        private IApplicationFilterServiceFactory ApplicationFilterServiceFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Tagging agent started");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var hub = EventHubFactory.Create(emptyReader);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    // Tenant token creation         
                    var token = TokenHandler.Issue(tenant.Id, "application-filters");
                    var reader = new StaticTokenReader(token.Value);

                    // Needed resources for this operation                    
                    var repository = RepositoryFactory.Create(reader);
                    var configuration = ApiConfigurationFactory.Create(reader).Get();

                    if (configuration == null)
                        throw new ArgumentException("Tagging events configuration cannot be found, please check");

                    var applicationFilterService = ApplicationFilterServiceFactory.Create(reader, logger);

                    // Attach all configured events to be listen
                    configuration
                    .Events                    
                    .ForEach(eventConfig => hub.On(eventConfig.Name, applicationFilterService.ProcessTaggingEvent));
                });
                hub.StartAsync();
            }
            catch (WebSocketSharp.WebSocketException ex)
            {
                logger.Error("Error while listening eventhub", ex);
                Start();
            }
        }
    }
}
