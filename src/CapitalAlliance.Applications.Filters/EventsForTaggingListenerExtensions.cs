﻿using CapitalAlliance.Applications.Filters.Abstractions.Services;
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace CapitalAlliance.Applications.Filters
{
    public static class EventsForTaggingListenerExtensions
    {
        public static void UseEventsForTaggingListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IEventsForTaggingListener>().Start();
        }
    }
}
