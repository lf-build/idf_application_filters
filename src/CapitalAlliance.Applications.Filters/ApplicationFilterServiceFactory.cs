﻿using CapitalAlliance.Applications.Filters.Abstractions;
using System;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration.Client;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using LendFoundry.Clients.DecisionEngine;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CreditExchange.StatusManagement.Client;

namespace CapitalAlliance.Applications.Filters
{
    public class ApplicationFilterServiceFactory : IApplicationFilterServiceFactory
    {
        public ApplicationFilterServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IServiceProvider Provider { get; }

        public IApplicationFilterServiceExtended Create(ITokenReader reader, ILogger logger)
        {
            var repositoryFactory = Provider.GetService<IFilterViewRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventHub = eventHubFactory.Create(reader);
            
            var configurationServiceFactory = Provider.GetService<IConfigurationServiceFactory>();
            var taggingConfigurationService = configurationServiceFactory.Create<TaggingConfiguration>(Settings.TaggingEvents, reader);
            var taggingConfiguration = taggingConfigurationService.Get();

            var configurationService = configurationServiceFactory.Create<Configuration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();

            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationServiceFactory, reader);

            var decisionEngineFactory = Provider.GetService<IDecisionEngineClientFactory>();
            var decisionEngine = decisionEngineFactory.Create(reader);

            var statusManagementServiceFactory = Provider.GetService<IStatusManagementServiceFactory>();
            var statusService = statusManagementServiceFactory.Create(reader);

            return new  ApplicationFilterService(repository, eventHub, logger, tenantTime, configuration, taggingConfiguration, decisionEngine, statusService);
        }
    }
}
