﻿using CapitalAlliance.Applications.Filters.Abstractions.Services;
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace CapitalAlliance.Applications.Filters
{
    public static class ApplicationFilterListenerExtensions
    {
        public static void UseApplicationFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IApplicationFilterListener>().Start();
        }
    }
}
