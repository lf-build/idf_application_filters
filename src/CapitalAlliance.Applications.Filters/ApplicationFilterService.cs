﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using System.Globalization;
using LendFoundry.Clients.DecisionEngine;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CapitalAlliance.Applications.Filters.Abstractions;
using Newtonsoft.Json.Linq;
using CreditExchange.StatusManagement.Client;
using CreditExchange.StatusManagement;
using System.Text;

namespace CapitalAlliance.Applications.Filters
{
    public class ApplicationFilterService : IApplicationFilterServiceExtended
    {
        public ApplicationFilterService
        (
            IFilterViewRepository repository,
            IEventHubClient eventHub,
            ILogger logger,
            ITenantTime tenantTime,
            Configuration configuration,
            TaggingConfiguration taggingConfiguration,
            IDecisionEngineService decisionEngineService,
            IEntityStatusService statusManagementServiceClient
        )
        {
            Repository = repository;
            EventHub = eventHub;
            Logger = logger;
            TenantTime = tenantTime;
            Configuration = configuration;
            TaggingConfiguration = taggingConfiguration;
            DecisionEngineService = decisionEngineService;
            StatusManagementServiceClient = statusManagementServiceClient;
        }

        private Configuration Configuration { get; }

        private TaggingConfiguration TaggingConfiguration { get; }

        private IFilterViewRepository Repository { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        private ITenantTime TenantTime { get; }

        private IDecisionEngineService DecisionEngineService { get; }

        private IEntityStatusService StatusManagementServiceClient { get; }

        public IEnumerable<IFilterView> GetAll()
        {
            return Repository.GetAll();
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validate(sourceType, sourceId);

            return Repository.GetAllBySource(sourceType, sourceId);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate(sourceType, sourceId);

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetBySourceAndStatus(sourceType, sourceId, statuses);
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            try
            {
                Validate(sourceType, sourceId);

                var firstDayOfMonth = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentMonth(sourceType, sourceId, firstDayOfMonth);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByMonth({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            try
            {
                Validate(sourceType, sourceId);

                var firstDayOfYear = new DateTimeOffset(new DateTime(TenantTime.Now.Year, 1, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentYear(sourceType, sourceId, firstDayOfYear);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetCountByYear({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        private void Validate(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            try
            {
                Validate(sourceType, sourceId);

                if (string.IsNullOrWhiteSpace(name))
                    throw new InvalidArgumentException($"{nameof(name)} is mandatory");

                return Repository.GetByName(sourceType, sourceId, name);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByName({sourceType}, {sourceId}, {name}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate(sourceType, sourceId);

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetByStatus(sourceType, sourceId, statuses);
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetAllByStatus(statuses);
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate(sourceType, sourceId);

            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");

            return Repository.GetCountByStatus(sourceType, sourceId, statuses);
        }

        public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            Validate(sourceType, sourceId);

            if (tags == null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");

            return Repository.GetByTag(sourceType, sourceId, tags);
        }

        public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            Validate(sourceType, sourceId);

            if (tags == null || !tags.Any())
                throw new ArgumentException($"{nameof(tags)} is mandatory");

            return Repository.GetCountByTag(sourceType, sourceId, tags);
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new ArgumentException($"{nameof(sourceType)} is mandatory");

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new ArgumentException($"{nameof(sourceId)} is mandatory");

            if (Configuration?.ApprovedStatuses == null || Configuration.ApprovedStatuses.Any() == false)
                throw new ArgumentException($"{nameof(Configuration.ApprovedStatuses)} is mandatory in configuration");

            return
                Repository.GetTotalAmountApprovedBySourceAndStatus(sourceType, sourceId, Configuration.ApprovedStatuses);
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            Validate(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

            var result = Repository.GetByApplicationNumber(sourceType, sourceId, applicationNumber);
            if (result.Result == null)
                throw new NotFoundException($"The snapshot could not be found with SourceType:{sourceType}, SourceId:{sourceId}, ApplicationNumber:{applicationNumber}");

            return result;
        }

        public async Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber)
        {
            var filterDataView = await GetByApplicationNumber(sourceType, sourceId, applicationNumber);
            var statusHistory = StatusManagementServiceClient.GetStatusTransitionHistory("application", applicationNumber);
            filterDataView.StatusHistory = TransformHistoryToDictionary(statusHistory);
            return filterDataView;
        }

        public async Task<bool> CheckIfDuplicateExists(UniqueParameterTypes parameterName, string parameterValue)
        {            
            if (string.IsNullOrEmpty(parameterValue))
                throw new ArgumentNullException($"required parameter {nameof(parameterValue)} missing");

            var result = false;
            var application = await Repository.GetUsingUniqueAttributes(parameterName, parameterValue);
            if(application != null && 
               Configuration.ReApplyStatuseCodes.Contains(application.StatusCode) && 
               DateTimeOffset.Now.Subtract(application.Submitted).Days >= Configuration.ReApplyTimeFrameDays)
            {
                result = false;
            }
            else if(application != null)
            {
                result = true;
            }
            return result; 
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            var todayDate = TenantTime.Today;
            return await Repository.GetAllExpiredApplications(todayDate);
        }

        #region "Tagging related methods"

        public void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentNullException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));
            if (tags == null || tags.Count() == 0)
                throw new ArgumentNullException($"{nameof(tags)} is mandatory", nameof(tags));

            ApplyTags(applicationNumber, tags);
        }

        public void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {            
            if (string.IsNullOrEmpty(applicationNumber))
                throw new ArgumentNullException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));
            if (tags == null || tags.Count() == 0)
                throw new ArgumentNullException($"{nameof(tags)} is mandatory", nameof(tags));

            RemoveTags(applicationNumber, tags);            
        }

        public void ProcessTaggingEvent(LendFoundry.EventHub.Client.EventInfo @event)
        {
            try
            {
                if (@event == null)
                    throw new InvalidArgumentException($"#{nameof(@event)} cannot be null", nameof(@event));

                var taggingEventConfiguration = TaggingConfiguration.Events.FirstOrDefault(x => x.Name == @event.Name);

                if (taggingEventConfiguration == null)
                    throw new ArgumentException($"Tagging event configuration for {@event.Name} not found");

                var applicationNumber = taggingEventConfiguration.ApplicationNumber.FormatWith(@event);
                Logger.Info($"Application Number : #{applicationNumber}");

                ApplyTags(applicationNumber, taggingEventConfiguration.ApplyTags);
                RemoveTags(applicationNumber, taggingEventConfiguration.RemoveTags);
                
                taggingEventConfiguration.Rules?.ToList().ForEach(x =>
                {
                    var ruleName = x.Name;
                    var version = x.Version;

                    try
                    {
                        var ruleResult = DecisionEngineService.Execute<dynamic, RuleResult>(ruleName, version, new { payload = @event.Data });
                        if(ruleResult.Output != null)
                        {
                            ApplyTags(applicationNumber, ruleResult.Output.ApplyTags);
                            RemoveTags(applicationNumber, ruleResult.Output.RemoveTags);
                        }

                    }catch(Exception ex)
                    {
                        Logger.Error($"Unhandled exception while processing application #{applicationNumber} for rule #{ruleName}.", ex);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error($"The method ProcessTaggingEvent({@event}) raised an error:{ex.Message}, Full Error:", ex);
            }
        }

        private void ApplyTags(string applicationNumber, IEnumerable<string> tags)
        {
            tags?.ToList().ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x))
                {
                    Repository.AddTag(applicationNumber, x);
                }
            });
        }

        private void RemoveTags(string applicationNumber, IEnumerable<string> tags)
        {
            tags?.ToList().ForEach(x =>
            {
                if (!string.IsNullOrEmpty(x))
                {
                    Repository.RemoveTag(applicationNumber, x);
                }
            });
        }
        #endregion

        private Dictionary<string, object> TransformHistoryToDictionary(List<IEntityStatus> statusHistory)
        {
            Dictionary<string, object> statusHistoryDictionary = null;
            if(statusHistory != null && statusHistory.Count > 0)
            {
                statusHistoryDictionary = new Dictionary<string, object>();
                foreach(var status in statusHistory)
                {
                    StringBuilder strReason = new StringBuilder();
                    if (status.Reason != null && status.Reason.Count > 0)
                    {
                        foreach (var reason in status.Reason)
                        {
                            strReason.Append(reason + ",");
                        }
                    }
                    
                    statusHistoryDictionary.Add(status.Status, new { ActivatedBy = status.ActivatedBy, ActiveOn = status.ActiveOn,
                                                                     Reasons = strReason.Length > 0 ? strReason.ToString().Substring(0, strReason.Length - 1) : string.Empty });
                }
            }
            return statusHistoryDictionary;
        }
    }
}
