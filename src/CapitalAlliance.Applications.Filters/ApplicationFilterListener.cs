﻿using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CreditExchange.StatusManagement;
using CreditExchange.StatusManagement.Client;
using LendFoundry.Business.Applicant;
using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using Configuration = CapitalAlliance.Applications.Filters.Abstractions.Configurations.Configuration;

namespace CapitalAlliance.Applications.Filters
{
    public class ApplicationFilterListener : IApplicationFilterListener
    {
        public ApplicationFilterListener
        (
            IConfigurationServiceFactory<Configuration> apiConfigurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IFilterViewRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IApplicationServiceClientFactory applicationServiceFactory,
            IApplicantServiceClientFactory applicantServiceFactory,            
            IStatusManagementServiceFactory statusManagementFactory,      
            IConfigurationServiceFactory configurationFactory,      
            ITenantTimeFactory tenantTimeFactory
            
        )
        {
            EventHubFactory = eventHubFactory;
            ApiConfigurationFactory = apiConfigurationFactory;
            TokenHandler = tokenHandler;
            RepositoryFactory = repositoryFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
            ApplicationServiceFactory = applicationServiceFactory;
            ApplicantServiceFactory = applicantServiceFactory;            
            StatusManagementFactory = statusManagementFactory;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;           
        }

        private IStatusManagementServiceFactory StatusManagementFactory { get; }
       
        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }

        private IApplicantServiceClientFactory ApplicantServiceFactory { get; }

        private IConfigurationServiceFactory<Configuration> ApiConfigurationFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }     


        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Eventhub listener started");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var hub = EventHubFactory.Create(emptyReader);
                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    // Tenant token creation         
                    var token = TokenHandler.Issue(tenant.Id, "application-filters");
                    var reader = new StaticTokenReader(token.Value);

                    // Needed resources for this operation
                    var statusManagementService = StatusManagementFactory.Create(reader);
                    var repository = RepositoryFactory.Create(reader);
                    var applicationService = ApplicationServiceFactory.Create(reader);
                    var applicantService = ApplicantServiceFactory.Create(reader);
                    var configuration = ApiConfigurationFactory.Create(reader).Get();
                    var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);                   

                    if (configuration == null)
                        throw new ArgumentException("Api configuration cannot be found, please check");

                    // Attach all configured events to be listen
                    configuration
                        .Events
                        .ToList()
                        .ForEach(eventConfig => hub.On(eventConfig.Name, AddView(eventConfig, repository, logger, applicationService, applicantService, statusManagementService, tenantTime)));
                });
                hub.StartAsync();
            }
            catch (WebSocketSharp.WebSocketException ex)
            {
                logger.Error("Error while listening eventhub", ex);
                Start();
            }
        }

        private static Action<EventInfo> AddView
        (
            EventMapping eventConfiguration,
            IFilterViewRepository repository,
            ILogger logger,
            LendFoundry.Business.Application.IApplicationService applicationService,
            IApplicantService applicantService,
            IEntityStatusService statusManagementService,
            ITenantTime tenantTime
        )
        {
            return async @event =>
            {
                var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith(@event);
                try
                {
                    logger.Info($"Application Number : #{applicationNumber}");

                    var application = await applicationService.GetByApplicationNumber(applicationNumber);

                    if (application == null)
                    {
                        logger.Warn($"No application found for application #{applicationNumber}");
                        return;
                    }

                    var applicant = await applicantService.Get(application.ApplicantId);

                    if (applicant == null)
                    {
                        logger.Warn($"No applicants found for application #{applicationNumber}");
                        return;
                    }
                                        
                    // snapshot creation
                    var data = new FilterView();     
                    data.ApplicationId = application.ApplicantId;
                    data.ApplicationNumber = application.ApplicationNumber;
                    if (applicant.Owners != null && applicant.Owners.Any())
                    {
                        data.Applicant = $"{applicant.Owners[0].FirstName} {applicant.Owners[0].LastName}";
                        data.ApplicantFirstName = applicant.Owners[0].FirstName;
                        data.ApplicantLastName = string.Empty;
                        data.ApplicantMiddleName = applicant.Owners[0].LastName;
                    }
                    data.AmountRequested = application.RequestedAmount;
                    data.RequestTermType = application.RequestedTermType.ToString();
                    data.RequestTermValue = application.RequestedTermValue;
                    data.ExpirationDate = application.ExpiryDate;                    

                    if (applicant.Addresses != null && applicant.Addresses.Count > 0)
                    {
                        var homeAddress =
                            applicant.Addresses.FirstOrDefault(x => x.AddressType == AddressType.Home);
                        if (homeAddress != null)
                        {
                            data.ApplicantAddressLine1 = homeAddress.AddressLine1;
                            data.ApplicantAddressLine2 = homeAddress.AddressLine2;
                            data.ApplicantAddressLine3 = homeAddress.AddressLine3;
                            data.ApplicantAddressLine4 = homeAddress.AddressLine4;
                            data.ApplicantAddressCity = homeAddress.City;
                            data.ApplicantAddressCountry = homeAddress.Country;
                            data.ApplicantAddressIsDefault = homeAddress.IsDefault;
                        }
                    }

                    var applicantPhone =
                        applicant.PhoneNumbers?.FirstOrDefault(x => x.PhoneType == PhoneType.Mobile);
                    if (applicantPhone != null)
                    {
                        data.ApplicantPhone = applicantPhone.Phone;
                    }

                    var applicantWorkEmail =
                       applicant.EmailAddresses?.FirstOrDefault(x => x.EmailType == EmailType.Work);
                    if (applicantWorkEmail != null)
                    {
                        data.ApplicantWorkEmail = applicantWorkEmail.Email;
                    }
                 
                    var emailAddress =
                        applicant.EmailAddresses?.FirstOrDefault(x => x.EmailType == EmailType.Personal);
                    if (emailAddress != null)
                    {
                        data.ApplicantPersonalEmail = emailAddress.Email;
                    }
                    if (application.Source != null)
                    {
                        data.SourceId = application.Source.SourceReferenceId;
                        data.SourceType = application.Source.SourceType.ToString();
                    }
                    data.Submitted = application.ApplicationDate;                    
                    
                    var applicationStatus = statusManagementService.GetStatusByEntity("application", applicationNumber);
                    if (applicationStatus != null)
                    {
                        data.StatusCode = applicationStatus.Code;
                        data.StatusName = applicationStatus.Name;
                        data.StatusDate = applicationStatus.ActiveOn.Time;
                        logger.Info($"Status found for this snapshot #{data.StatusCode} #{data.StatusName}");
                    }
                    else
                        logger.Info($"No status found for this snapshot");

                    if (eventConfiguration.UpdateLastProgressDate)
                    {
                        data.LastProgressDate = tenantTime.Today;
                    }

                    data.Tags = GetTags();
                    repository.AddOrUpdate(data);
                    logger.Info($"New snapshot added to application {applicationNumber}");
                }
                catch (Exception ex) { logger.Error($"Unhadled exception while listening event {@event.Name}", ex); }
            };
        }
                
        private static IEnumerable<string> GetTags()
        {
            // Tag Creation stub - will be updated once rule engine available
            var appTags = new List<string>();
            return appTags;
        }

    }
}
