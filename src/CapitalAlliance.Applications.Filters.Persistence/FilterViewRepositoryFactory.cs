﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using CapitalAlliance.Applications.Filters.Abstractions.Services;

namespace CapitalAlliance.Applications.Filters.Persistence
{
    public class FilterViewRepositoryFactory : IFilterViewRepositoryFactory
    {
        public FilterViewRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new FilterViewRepository(tenantService, mongoConfiguration);
        }
    }
}
