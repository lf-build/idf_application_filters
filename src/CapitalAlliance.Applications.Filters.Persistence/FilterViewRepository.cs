﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {
            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Submitted).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                //map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(FilterView);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "application-filters")
        {
            CreateIndexIfNotExists("applicant", Builders<IFilterView>.IndexKeys.Ascending(i => i.Applicant));
            CreateIndexIfNotExists("status-code", Builders<IFilterView>.IndexKeys.Ascending(i => i.StatusCode));
            CreateIndexIfNotExists("status-name", Builders<IFilterView>.IndexKeys.Ascending(i => i.StatusName));
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));

            Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.ApplicationNumber == view.ApplicationNumber;

            var existingView = Query
                .Where(v => v.ApplicationNumber == view.ApplicationNumber && v.TenantId == TenantService.Current.Id)                
                .FirstOrDefault();

            view.TenantId = TenantService.Current.Id;
            if (existingView != null && !string.IsNullOrEmpty(existingView.Id))
                view.Id = existingView.Id;
            else
                view.Id = ObjectId.GenerateNewId().ToString();
            if (existingView != null && existingView.Tags != null && existingView.Tags.Any())
            {
                view.Tags = view.Tags.Union(existingView.Tags);
            }

            Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return OrderIt(Query);
        }

        public IEnumerable<IFilterView> GetByStatus(IEnumerable<string> statuses)
        {
            return OrderIt(Query.Where(x => statuses.Contains(x.StatusCode)));
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId));
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId && statuses.Contains(x.StatusCode)));
        }

        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode)
                             .ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentMonth
                ));
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentYear
                ));
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(
                OrderIt(
                    Query.Where(a => a.SourceType.ToLower() == sourceType.ToLower()
                                    && a.SourceId == sourceId
                                    && a.Applicant.ToLower().Contains(name.ToLower()))));
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId &&
                                         statuses.Contains(x.StatusCode)))
            );
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(Query.Where(x => statuses.Contains(x.StatusCode)))
            );
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<int>
            (
                Query.Count(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId &&
                                         statuses.Contains(x.StatusCode))
            );
        }

        public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(from db in Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
                        from dbTags in db.Tags
                        where (from tag in tags select tag).Contains(dbTags)
                        select db).Distinct()
            );
        }

        public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return Task.FromResult<int>
            (
                (from db in Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
                 from dbTags in db.Tags
                 where (from tag in tags select tag).Contains(dbTags)
                 select db).Distinct().Count()
            );
        }

        public double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses)
        {
            return Query.Where(x => x.SourceType == sourceType && x.SourceId == sourceId && statuses.Contains(x.StatusCode))
                        .Sum(x => x.InitialOfferAmount);
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return Task.FromResult
                (
                    Query.FirstOrDefault(a => a.SourceType.ToLower() == sourceType.ToLower() &&
                        a.SourceId == sourceId &&
                        a.ApplicationNumber == applicationNumber)
                );
        }

        public Task<IFilterView> GetByApplicationNumber(string applicationNumber)
        {
            return Task.FromResult
                (
                    Query.FirstOrDefault(a => a.ApplicationNumber == applicationNumber)
                );
        }

        public Task<IFilterView> GetUsingUniqueAttributes(UniqueParameterTypes parameterType, string parameterValue)
        {
            Task<IFilterView> returnVal = null;
            switch(parameterType)
            {
                case UniqueParameterTypes.Email:
                    returnVal = Task.FromResult(Query.OrderByDescending(x => x.Submitted)?.FirstOrDefault(a => a.ApplicantPersonalEmail.ToLower() == parameterValue.ToLower()));
                    break;
                case UniqueParameterTypes.Mobile:
                    returnVal = Task.FromResult(Query.OrderByDescending(x => x.Submitted)?.FirstOrDefault(a => a.ApplicantPhone == parameterValue));
                    break;
                case UniqueParameterTypes.Pan:
                    returnVal = Task.FromResult(Query.OrderByDescending(x => x.Submitted)?.FirstOrDefault(a => a.ApplicantPanNumber.ToUpper() == parameterValue.ToUpper()));
                    break;
            }
            return returnVal;
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate)
        {

            return await Task.FromResult
            (
               Query.Where(p => p.ExpirationDate < todayDate).ToList()
            );
        }

        public void AddTag(string applicationNumber, string tag)
        {
            var tenantId = TenantService.Current.Id;

            Collection.UpdateOne(s =>
                    s.ApplicationNumber == applicationNumber &&
                    s.TenantId == tenantId,
                new UpdateDefinitionBuilder<IFilterView>()
                    .AddToSet<string>(a => a.Tags, tag)
                , new UpdateOptions() { IsUpsert = true });
        }

        public void RemoveTag(string applicationNumber, string tag)
        {
            var tenantId = TenantService.Current.Id;

            Collection.UpdateOne(s =>
                    s.ApplicationNumber == applicationNumber &&
                    s.TenantId == tenantId,
                new UpdateDefinitionBuilder<IFilterView>()
                    .Pull<string>(a => a.Tags, tag)
                , new UpdateOptions() { IsUpsert = true });
        }
    }
}