﻿namespace CapitalAlliance.Applications.Filters.Abstractions.Configurations
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string ApplicationNumber { get; set; }
        public bool UpdateLastProgressDate { get; set; }
    }
}
