﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters.Abstractions.Configurations
{
    public class TaggingEvent
    {
        public string Name { get; set; }
        public string ApplicationNumber { get; set; }
        public List<string> ApplyTags { get; set; }
        public List<string> RemoveTags { get; set; }
        public List<Rule> Rules { get; set; }
    }
}
