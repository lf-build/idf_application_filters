﻿using LendFoundry.EventHub.Client;

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IApplicationFilterServiceExtended : IApplicationFilterService
    {
        void ProcessTaggingEvent(EventInfo @event);
    }
}
