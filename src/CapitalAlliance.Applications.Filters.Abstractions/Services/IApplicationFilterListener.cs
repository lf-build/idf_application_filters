﻿namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IApplicationFilterListener
    {
        void Start();
    }
}
