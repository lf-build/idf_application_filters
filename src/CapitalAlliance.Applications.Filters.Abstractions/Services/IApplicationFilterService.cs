﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IApplicationFilterService
    {
        IEnumerable<IFilterView> GetAll();
        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId);

        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId);

        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);

        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags);

        Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags);

        double GetTotalAmountApprovedBySource(string sourceType, string sourceId);

        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);

        Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber);

        Task<bool> CheckIfDuplicateExists(UniqueParameterTypes parameterName, string parameterValue);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications();

        void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);

        void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);
    }
}
