﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        IEnumerable<IFilterView> GetAll();
        void AddOrUpdate(IFilterView view);
        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);

        Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth);

        Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear);

        Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags);

        Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags);

        double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses);

        Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber);

        Task<IFilterView> GetByApplicationNumber(string applicationNumber);

        Task<IFilterView> GetUsingUniqueAttributes(UniqueParameterTypes parameterName, string parameterValue);

        Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate);

        void AddTag(string applicationNumber, string tag);

        void RemoveTag(string applicationNumber, string tag);
    }
}