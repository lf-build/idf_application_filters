﻿using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}
