﻿namespace CapitalAlliance.Applications.Filters.Abstractions.Services
{
    public interface IEventsForTaggingListener
    {
        void Start();
    }
}
