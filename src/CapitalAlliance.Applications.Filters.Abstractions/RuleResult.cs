﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public class RuleResult
    {
        public List<string> ApplyTags { get; set; }
        public List<string> RemoveTags { get; set; }
    }
}
