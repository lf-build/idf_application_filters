﻿using CreditExchange.StatusManagement;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters
{
    public interface IFilterView : IAggregate
    {
        string ApplicationId { get; set; }
        string ApplicationNumber { get; set; }
        string Applicant { get; set; }
        double AmountRequested { get; set; }
        string RequestTermType { get; set; }
        double RequestTermValue { get; set; }
        string ApplicantFirstName { get; set; }
        string ApplicantLastName { get; set; }
        string ApplicantMiddleName { get; set; }
        string ApplicantPhone { get; set; }
        string ApplicantWorkEmail { get; set; }
        string ApplicantPersonalEmail { get; set; }
        string ApplicantPanNumber { get; set; }
        string ApplicantAadharNumber { get; set; }
        string ApplicantEmployer { get; set; }
        string ApplicantAddressLine1 { get; set; }
        string ApplicantAddressLine2 { get; set; }
        string ApplicantAddressLine3 { get; set; }
        string ApplicantAddressLine4 { get; set; }
        string ApplicantAddressCity { get; set; }
        string ApplicantAddressCountry { get; set; }
        bool ApplicantAddressIsDefault { get; set; }
        DateTimeOffset Submitted { get; set; }
        DateTimeOffset ExpirationDate { get; set; }
        DateTimeOffset LastProgressDate { get; set; }

        string StatusCode { get; set; }
        string StatusName { get; set; }
        DateTimeOffset StatusDate { get; set; }

        string SourceId { get; set; }
        string SourceType { get; set; }

        string FileType { get; set; }
        double InitialOfferAmount { get; set; }
        double InitialOfferInterestRate { get; set; }
        int InitialOfferLoanTenure { get; set; }
        double InitialOfferScore { get; set; }

        double FinalOfferAmount { get; set; }
        double FinalOfferInterestRate { get; set; }
        double FinalOfferInstallmentAmount { get; set; }
        string FinalOfferDurationInMonths { get; set; }
        double FinalOfferProcessingFee { get; set; }

        IEnumerable<string> Tags { get; set; }

        Dictionary<string, object> StatusHistory { get; set; }
    }
}