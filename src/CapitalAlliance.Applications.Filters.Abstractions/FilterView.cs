﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace CapitalAlliance.Applications.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public string ApplicationId { get; set; }
        public string ApplicationNumber { get; set; }
        public string Applicant { get; set; }
        public double AmountRequested { get; set; }
        public string RequestTermType { get; set; }
        public double RequestTermValue { get; set; }
        public string ApplicantFirstName { get; set; }
        public string ApplicantLastName { get; set; }
        public string ApplicantMiddleName { get; set; }
        public string ApplicantPhone { get; set; }
        public string ApplicantWorkEmail { get; set; }
        public string ApplicantPersonalEmail { get; set; }
        public string ApplicantPanNumber { get; set; }
        public string ApplicantAadharNumber { get; set; }
        public string ApplicantEmployer { get; set; }
        public string ApplicantAddressLine1 { get; set; }
        public string ApplicantAddressLine2 { get; set; }
        public string ApplicantAddressLine3 { get; set; }
        public string ApplicantAddressLine4 { get; set; }
        public string ApplicantAddressCity { get; set; }
        public string ApplicantAddressCountry { get; set; }
        public bool ApplicantAddressIsDefault { get; set; }
        public DateTimeOffset Submitted { get; set; }
        public DateTimeOffset ExpirationDate { get; set; }
        public DateTimeOffset LastProgressDate { get; set; }

        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public DateTimeOffset StatusDate { get; set; }

        public string SourceId { get; set; }
        public string SourceType { get; set; }

        public string FileType { get; set; }
        public double InitialOfferAmount { get; set; }
        public double InitialOfferInterestRate { get; set; }        
        public int InitialOfferLoanTenure { get; set; }
        public double InitialOfferScore { get; set; }
                
        public double FinalOfferAmount { get; set; }
        public double FinalOfferInterestRate { get; set; }
        public double FinalOfferInstallmentAmount { get; set; }
        public string FinalOfferDurationInMonths { get; set; }
        public double FinalOfferProcessingFee { get; set; }
              
        public IEnumerable<string> Tags { get; set; }
        public Dictionary<string, object> StatusHistory { get; set; }
    }
}