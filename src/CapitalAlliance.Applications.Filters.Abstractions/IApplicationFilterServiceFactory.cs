﻿using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Applications.Filters.Abstractions
{
    public interface IApplicationFilterServiceFactory
    {
        IApplicationFilterServiceExtended Create(ITokenReader reader, ILogger logger);
    }
}
