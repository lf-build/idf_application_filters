﻿using LendFoundry.Business.Applicant.Client;
using LendFoundry.Business.Application.Client;
using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CapitalAlliance.Applications.Filters.Persistence;
using CreditExchange.StatusManagement.Client;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace CapitalAlliance.Applications.Filters.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddConfigurationService<TaggingConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.TaggingEvents);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);
            services.AddApplicantService(Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
          
            // interface resolvers
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<TaggingConfiguration>>().Get());
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IApplicationFilterService, ApplicationFilterService>();
            services.AddTransient<IFilterViewRepository, FilterViewRepository>();
            services.AddTransient<IFilterViewRepositoryFactory, FilterViewRepositoryFactory>();
            services.AddTransient<IApplicationFilterListener, ApplicationFilterListener>();
            services.AddTransient<IApplicationFilterServiceFactory, ApplicationFilterServiceFactory>();
            services.AddTransient<IEventsForTaggingListener, EventsForTaggingListener>();

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();            
            app.UseEventHub();
            app.UseApplicationFilterListener();
            app.UseEventsForTaggingListener();
            app.UseMvc();
        }
    }
}
