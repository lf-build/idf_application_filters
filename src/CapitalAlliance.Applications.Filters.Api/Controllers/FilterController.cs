﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Api.Controllers
{
    [Route("/")]
    public class FilterController : ExtendedController
    {
        public FilterController(IApplicationFilterService service)
        {
            Service = service;
        }

        private IApplicationFilterService Service { get; }

        [HttpGet("/all")]
        public IActionResult GetAll()
        {
            return Execute(() => new HttpOkObjectResult(Service.GetAll()));
        }

        [HttpGet("/{sourceType}/{sourceId}/all")]
        public IActionResult GetAllBySource(string sourceType, string sourceId)
        {
            return Execute(() => new HttpOkObjectResult(Service.GetAllBySource(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/submit/month")]
        public Task<IActionResult> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetTotalSubmittedForCurrentMonth(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/submit/year")]
        public Task<IActionResult> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetTotalSubmittedForCurrentYear(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/status/{*statuses}")]
        public Task<IActionResult> GetByStatus(string sourceType, string sourceId, string statuses)
        {
            return ExecuteAsync(async () =>
            {
                statuses = WebUtility.UrlDecode(statuses);
                var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetByStatus(sourceType, sourceId, listStatus));
            });
        }

        [HttpGet("status/{*statuses}")]
        public Task<IActionResult> GetAllByStatus(string statuses)
        {
            return ExecuteAsync(async () =>
            {
                statuses = WebUtility.UrlDecode(statuses);
                var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetAllByStatus(listStatus));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/count/status/{*statuses}")]
        public Task<IActionResult> GetCountByStatus(string sourceType, string sourceId, string statuses)
        {
            return ExecuteAsync(async () =>
            {
                statuses = WebUtility.UrlDecode(statuses);
                var listStatus = statuses?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetCountByStatus(sourceType, sourceId, listStatus));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/tag/{*tags}")]
        public Task<IActionResult> GetByTag(string sourceType, string sourceId, string tags)
        {
            return ExecuteAsync(async () =>
            {
                tags = WebUtility.UrlDecode(tags);
                var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetByTag(sourceType, sourceId, listTags));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/count/tag/{*tags}")]
        public Task<IActionResult> GetCountByTag(string sourceType, string sourceId, string tags)
        {
            return ExecuteAsync(async () =>
            {
                tags = WebUtility.UrlDecode(tags);
                var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return new HttpOkObjectResult(await Service.GetCountByTag(sourceType, sourceId, listTags));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/search/{name}")]
        public Task<IActionResult> GetByName(string sourceType, string sourceId, string name)
        {
            return ExecuteAsync(async () =>
            {
                name = WebUtility.UrlDecode(name);
                return new HttpOkObjectResult(await Service.GetByName(sourceType, sourceId, name));
            });
        }

        [HttpGet("/{sourceType}/{sourceId}/metrics/total-approved-amount")]
        public IActionResult GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            return Execute(() => Ok(Service.GetTotalAmountApprovedBySource(sourceType, sourceId)));
        }

        [HttpGet("/{sourceType}/{sourceId}/{applicationNumber}")]
        public Task<IActionResult> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetByApplicationNumber(sourceType, sourceId, applicationNumber)));
        }

        [HttpGet("getwithstatushistory/{sourceType}/{sourceId}/{applicationNumber}")]
        public Task<IActionResult> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber)
        {
            return ExecuteAsync(async () => new HttpOkObjectResult(await Service.GetByApplicationNumberWithStatusHistory(sourceType, sourceId, applicationNumber)));
        }

        [HttpGet("duplicateexists/{parametertype}/{parametervalue}")]
        public async Task<IActionResult> DuplicateExists(UniqueParameterTypes parametertype, string parametervalue)
        {
            try
            {
                return await ExecuteAsync(async () =>
                {
                    return new HttpOkObjectResult(await Service.CheckIfDuplicateExists(parametertype, parametervalue));
                });
                
            }catch(Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }            
        }

        [HttpGet("/expired/all")]
        public async Task<IActionResult> GetAllExpiredApplications()
        {
            return await ExecuteAsync(async () =>
            {
                return new HttpOkObjectResult(await Service.GetAllExpiredApplications());
            });
        }

        [HttpPost("applytags/{applicationnumber}/{*tags}")]
        public IActionResult ApplyTagsToApplication(string applicationnumber, string tags)
        {
            try
            {
                return Execute(() =>
                {
                    tags = WebUtility.UrlDecode(tags);
                    var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.TagsWithoutEvaluation(applicationnumber, listTags);
                    return new HttpOkResult();
                });
            }
            catch(ArgumentNullException ex)
            {
                return new ErrorResult(404, ex.Message);
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }

        [HttpPost("removetags/{applicationnumber}/{*tags}")]
        public IActionResult RemoveTagsFromApplication(string applicationnumber, string tags)
        {
            try
            {
                return Execute(() =>
                {
                    tags = WebUtility.UrlDecode(tags);
                    var listTags = tags?.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                    Service.UnTagWithoutEvaluation(applicationnumber, listTags);
                    return new HttpOkResult();
                });
            }
            catch (ArgumentNullException ex)
            {
                return new ErrorResult(404, ex.Message);
            }
            catch (Exception ex)
            {
                return new ErrorResult(500, ex.Message);
            }
        }
    }
}
