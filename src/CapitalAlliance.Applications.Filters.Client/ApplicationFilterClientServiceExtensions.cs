﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace CapitalAlliance.Applications.Filters.Client
{
    public static class ApplicationFilterServiceExtension
    {
        public static IServiceCollection AddApplicationsFilterService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<IApplicationFilterClientServiceFactory>(p => new ApplicationFilterClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationFilterClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
