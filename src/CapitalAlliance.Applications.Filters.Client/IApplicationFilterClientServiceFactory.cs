﻿using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Security.Tokens;

namespace CapitalAlliance.Applications.Filters.Client
{
    public interface IApplicationFilterClientServiceFactory
    {
        IApplicationFilterService Create(ITokenReader reader);
    }
}
