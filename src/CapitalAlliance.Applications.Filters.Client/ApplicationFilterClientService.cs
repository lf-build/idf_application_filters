﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System;

namespace CapitalAlliance.Applications.Filters.Client
{
    public class ApplicationFilterClientService : IApplicationFilterService
    {
        public ApplicationFilterClientService(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAll()
        {
            var request = new RestRequest("/all", Method.GET);
            return Client.Execute<List<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/all", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return Client.Execute<List<FilterView>>(request);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return Client.Execute<List<FilterView>>(request);
        }

        public async Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/month", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return await Client.ExecuteAsync<int>(request);            
        }

        public async Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            Validade(sourceType, sourceId);

            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/submit/year", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);

            return await Client.ExecuteAsync<int>(request);            
        }

        public async Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(name))
                throw new InvalidArgumentException($"{nameof(name)} is mandatory", nameof(name));

            var request = new RestRequest("/{sourceType}/{sourceId}/search/{name}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(name), name);

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/count/status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);

            return await Client.ExecuteAsync<int>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/tag/{*tags}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in tags) strBuilder.Append($"{item}/");
            request.AddUrlSegment("tags", strBuilder.ToString());
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public async Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            Validade(sourceType, sourceId);
            var request = new RestRequest("/{sourceType}/{sourceId}/count/tag/{*tags}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in tags) strBuilder.Append($"{item}/");
            request.AddUrlSegment("tags", strBuilder.ToString());
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return await Client.ExecuteAsync<int>(request);

            //var result = Client.ExecuteAsync(request);
            //if (result == null)
            //    return Task.FromResult(0);

            //return Task.FromResult(Convert.ToInt32(result.Result));
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            var request = new RestRequest("/{sourceType}/{sourceId}/metrics/total-approved-amount", Method.GET);
            request.AddUrlSegment("sourceType", sourceType);
            request.AddUrlSegment("sourceId", sourceId);
            return Client.Execute<double>(request);
            //if (result == null)
            //    return 0;

            //return Convert.ToDouble(result);
        }

        public async Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            Validade(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

            var request = new RestRequest("/{sourceType}/{sourceId}/{applicationNumber}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);

            return await Client.ExecuteAsync<FilterView>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            var request = new RestRequest("/expired/all", Method.GET);
            var strBuilder = new StringBuilder();

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        private void Validade(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));
        }

        public async Task<bool> CheckIfDuplicateExists(UniqueParameterTypes parameterType, string parameterValue)
        {
            var request = new RestRequest("duplicateexists/{parametertype}/{parametervalue}", Method.GET);
            request.AddUrlSegment("parametertype", parameterType.ToString());
            request.AddUrlSegment("parametervalue", parameterValue);
            
            return await Client.ExecuteAsync<bool>(request);
        }

        public async Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("status/{*statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            var request = new RestRequest("applytags/{applicationnumber}/{*tags}", Method.POST);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            
            var strBuilder = new StringBuilder();
            foreach (var item in tags) strBuilder.Append($"{item}/");
            request.AddUrlSegment("tags", strBuilder.ToString());
            Client.Execute(request);
        }

        public void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            var request = new RestRequest("removetags/{applicationnumber}/{*tags}}", Method.POST);
            request.AddUrlSegment("applicationnumber", applicationNumber);
            var strBuilder = new StringBuilder();
            foreach (var item in tags) strBuilder.Append($"{item}/");
            request.AddUrlSegment("tags", strBuilder.ToString());
            Client.Execute(request);
        }

        public async Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber)
        {            
            var request = new RestRequest("getwithstatushistory/{sourceType}/{sourceId}/{applicationNumber}", Method.GET);
            request.AddUrlSegment(nameof(sourceType), sourceType);
            request.AddUrlSegment(nameof(sourceId), sourceId);
            request.AddUrlSegment(nameof(applicationNumber), applicationNumber);

            return await Client.ExecuteAsync<IFilterView>(request);
        }
    }
}
