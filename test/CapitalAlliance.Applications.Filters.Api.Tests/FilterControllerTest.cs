﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using CapitalAlliance.Applications.Filters.Api.Controllers;
using CapitalAlliance.Applications.Filters.Tests;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CapitalAlliance.Applications.Filters.Api.Tests
{
    public class FilterControllerTest
    {
        private const string DummyApplicantId = "123";

        private const string DummyApplicationNumber = "1234";

        private static IApplicationFilterService GetApplicationFilterService(List<IFilterView> filterViews = null)
        {
            return new FakeApplicationFilterService(GetFakeApplicationFilterViewRepository(filterViews), Mock.Of<IEventHubClient>(), Mock.Of<ILogger>(), Mock.Of<ITenantTime>(), GetConfiguration());
        }

        private static FakeFilterViewRepository GetFakeApplicationFilterViewRepository(List<IFilterView> filterViews = null)
        {
            return new FakeFilterViewRepository(new UtcTenantTime(), filterViews ?? new List<IFilterView>());
        }

        private static Configuration GetConfiguration()
        {
            return new Configuration
            {
                ApprovedStatuses = new string[] { "" },
                Events = new[] {
                    new EventMapping
                    {
                        ApplicationNumber = "",
                        Name = ""
                    }
                },
                Tags = new Dictionary<string, Rule>()
            };
        }

        private static FilterController GetController(List<IFilterView> filterViews = null)
        {
            return new FilterController(GetApplicationFilterService(filterViews));
        }

        private static List<IFilterView> GetFilterViewObject()
        {
            return new List<IFilterView>
            {
                new FilterView
                    {
                        Id = "12345",
                        Applicant = DummyApplicantId,
                        AmountRequested = 100.00,
                        ApplicantAadharNumber = "",
                        ApplicantAddressCity = "",
                        ApplicantAddressCountry = "",
                        ApplicantAddressIsDefault = true,
                        ApplicantAddressLine1 = "",
                        ApplicantAddressLine2 = "",
                        ApplicantAddressLine3 = "",
                        ApplicantAddressLine4 = "",
                        ApplicantEmployer = "",
                        ApplicantFirstName = "abc",
                        ApplicantLastName = "efg",
                        ApplicantMiddleName = "d",
                        ApplicantPanNumber = "BPZYPP0010",
                        ApplicantPersonalEmail = "abc@gmail.com",
                        ApplicantPhone = "9876543210",
                        ApplicantWorkEmail = "",
                        ApplicationId = "1234",
                        ApplicationNumber = DummyApplicationNumber,
                        ExpirationDate = DateTime.Now,
                        FinalOfferAmount = 100.00,
                        FinalOfferInstallmentAmount = 100.00,
                        FinalOfferInterestRate = 12.50,
                        InitialOfferAmount = 100.00,
                        InitialOfferInterestRate = 10.25,
                        LastProgressDate = DateTime.Now,
                        RequestTermType = "",
                        RequestTermValue = 150.00,
                        SourceId = "SourceId",
                        SourceType = "Merchant",
                        StatusCode = "InProgress",
                        StatusDate = DateTime.Now,
                        StatusName = "",
                        Submitted = DateTime.Now.AddDays(10),
                        Tags = new List<string> { "Tag1" },
                        TenantId = "Tenant1"
                    }
            };
        }

        [Fact]
        public void Should_GetAll_WithValidRequest_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = filterController.GetAll();
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterViews = ((HttpOkObjectResult)response).Value as IEnumerable<IFilterView>;
            Assert.NotNull(responseFilterViews);
            Assert.Equal(1, responseFilterViews.Count());
            Assert.Equal(DummyApplicantId, responseFilterViews.Select(f=>f.Applicant).FirstOrDefault());
        }

        [Fact]
        public void Should_GetAllBySource_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = filterController.GetAllBySource("Merchant", "SourceId");
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterViews = ((HttpOkObjectResult)response).Value as IEnumerable<IFilterView>;
            Assert.NotNull(responseFilterViews);
            Assert.Equal(1, responseFilterViews.Count());
            Assert.Equal(DummyApplicantId, responseFilterViews.Select(f => f.Applicant).FirstOrDefault());
        }

        [Fact]
        public async void Should_GetTotalSubmittedForCurrentMonth_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetTotalSubmittedForCurrentMonth("Merchant", "SourceId");
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as int?;
            Assert.NotNull(result);
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Should_GetTotalSubmittedForCurrentYear_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetTotalSubmittedForCurrentYear("Merchant", "SourceId");
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as int?;
            Assert.NotNull(result);
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Should_GetByStatus_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetByStatus("Merchant", "SourceId", "InProgress");
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterViews = ((HttpOkObjectResult)response).Value as IEnumerable<IFilterView>;
            Assert.NotNull(responseFilterViews);
            Assert.Equal(1, responseFilterViews.Count());
            Assert.Equal(DummyApplicantId, responseFilterViews.Select(f => f.Applicant).FirstOrDefault());
        }

        [Fact]
        public async void Should_GetCountByStatus_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetCountByStatus("Merchant", "SourceId", "InProgress");
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as int?;
            Assert.NotNull(result);
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Should_GetByTag_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetByTag("Merchant", "SourceId", "Tag1");
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterViews = ((HttpOkObjectResult)response).Value as IEnumerable<IFilterView>;
            Assert.NotNull(responseFilterViews);
            Assert.Equal(1, responseFilterViews.Count());
            Assert.Equal(DummyApplicantId, responseFilterViews.Select(f => f.Applicant).FirstOrDefault());
        }

        [Fact]
        public async void Should_GetCountByTag_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetCountByTag("Merchant", "SourceId", "Tag1");
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as int?;
            Assert.NotNull(result);
            Assert.Equal(1, result);
        }

        [Fact]
        public async void Should_GetByName_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetByName("Merchant", "SourceId", "123");
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterViews = ((HttpOkObjectResult)response).Value as IEnumerable<IFilterView>;
            Assert.NotNull(responseFilterViews);
            Assert.Equal(1, responseFilterViews.Count());
            Assert.Equal(DummyApplicantId, responseFilterViews.Select(f => f.Applicant).FirstOrDefault());
        }

        [Fact]
        public void Should_GetTotalAmountApprovedBySource_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = filterController.GetTotalAmountApprovedBySource("Merchant", "SourceId");
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as double?;
            Assert.NotNull(result);
            Assert.Equal(100.00, result);
        }

        [Fact]
        public async void Should_GetByApplicationNumber_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetByApplicationNumber("Merchant", "SourceId", "1234");
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterView = ((HttpOkObjectResult)response).Value as IFilterView;
            Assert.NotNull(responseFilterView);
            Assert.Equal(DummyApplicationNumber, responseFilterView.ApplicationNumber);
        }

        [Fact]
        public async void Should_DuplicateExists_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.DuplicateExists(UniqueParameterTypes.Email, "abc@gmail.com");            
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as bool?;
            Assert.NotNull(result);
            Assert.Equal(true, result);
        }

        [Fact]
        public async void Should_GetAllExpiredApplications_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetAllExpiredApplications();
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterViews = ((HttpOkObjectResult)response).Value as IEnumerable<IFilterView>;
            Assert.NotNull(responseFilterViews);
            Assert.Equal(1, responseFilterViews.Count());
            Assert.Equal(DummyApplicantId, responseFilterViews.Select(f => f.Applicant).FirstOrDefault());
        }

        [Fact]
        public async void Should_GetAllByStatus_WithValidParameters_ReturnOk()
        {
            var filterController = GetController(GetFilterViewObject());
            var response = await filterController.GetAllByStatus("InProgress");
            Assert.IsType<HttpOkObjectResult>(response);
            var responseFilterViews = ((HttpOkObjectResult)response).Value as IEnumerable<IFilterView>;
            Assert.NotNull(responseFilterViews);
            Assert.Equal(1, responseFilterViews.Count());
            Assert.Equal(DummyApplicantId, responseFilterViews.Select(f => f.Applicant).FirstOrDefault());
        }
    }
}
