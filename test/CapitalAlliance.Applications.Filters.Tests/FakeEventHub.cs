﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;

namespace CapitalAlliance.Applications.Filters.Tests
{
    public class FakeEventHub : IEventHubClient
    {
        internal List<Tuple<string, Action<EventInfo>>> Actions { get; } = new List<Tuple<string, Action<EventInfo>>>();

        public Task<bool> Publish<T>(string eventName, T data)
        {
            Actions.ForEach(tuple =>
                {
                    if (tuple.Item1 == eventName)
                    {
                        tuple.Item2(new EventInfo()
                        {
                            Data = data,
                            Name = eventName
                        });
                    }
                }
            );
            return Task.FromResult(true);
        }

        public void PublishBatchWithInterval<T>(List<T> events, int interval = 100)
        {
            throw new NotImplementedException();
        }

        public void On(string eventName, Action<EventInfo> handler)
        {
            Actions.Add(new Tuple<string, Action<EventInfo>>(eventName, handler));
        }

        public void Start()
        {
            while (IsExistRequested==false)
            {
                Thread.Sleep(100);
            }
        }
        private bool IsExistRequested { get; set; }
        public void Close()
        {
            IsExistRequested = true;
        }

        public async void StartAsync()
        {
            await Task.Run(() => Start());
        }

        public Task<bool> Publish<T>(T @event)
        {
            return new Task<bool>(() => true);
        }
    }
}