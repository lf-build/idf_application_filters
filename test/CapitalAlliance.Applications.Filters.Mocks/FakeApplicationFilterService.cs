﻿using CapitalAlliance.Applications.Filters.Abstractions;
using CapitalAlliance.Applications.Filters.Abstractions.Configurations;
using CapitalAlliance.Applications.Filters.Abstractions.Services;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CapitalAlliance.Applications.Filters.Tests
{
    public class FakeApplicationFilterService : IApplicationFilterService
    {
        public ITenantTime TenantTime { get; set; }
        public List<IFilterView> FilterViews { get; } = new List<IFilterView>();

        public IFilterViewRepository FakeFilterViewRepository { get; }

        public FakeApplicationFilterService(IFilterViewRepository repository,
            IEventHubClient eventHub,
            ILogger logger,
            ITenantTime tenantTime,
            Configuration configuration)
        {
            FakeFilterViewRepository = repository;
        }

        private static List<IFilterView> GetFilterViewObject()
        {
            return new List<IFilterView>
            {
                new FilterView
                    {
                        Id = "12345",
                        Applicant = "123",
                        AmountRequested = 100.00,
                        ApplicantAadharNumber = "",
                        ApplicantAddressCity = "",
                        ApplicantAddressCountry = "",
                        ApplicantAddressIsDefault = true,
                        ApplicantAddressLine1 = "",
                        ApplicantAddressLine2 = "",
                        ApplicantAddressLine3 = "",
                        ApplicantAddressLine4 = "",
                        ApplicantEmployer = "",
                        ApplicantFirstName = "",
                        ApplicantLastName = "",
                        ApplicantMiddleName = "",
                        ApplicantPanNumber = "",
                        ApplicantPersonalEmail = "",
                        ApplicantPhone = "",
                        ApplicantWorkEmail = "",
                        ApplicationId = "1234",
                        ApplicationNumber = "1234",
                        ExpirationDate = DateTime.Now,
                        FinalOfferAmount = 100.00,
                        FinalOfferInstallmentAmount = 100.00,
                        FinalOfferInterestRate = 12.50,
                        InitialOfferAmount = 100.00,
                        InitialOfferInterestRate = 10.25,
                        LastProgressDate = DateTime.Now,
                        RequestTermType = "",
                        RequestTermValue = 150.00,
                        SourceId = "SourceId",
                        SourceType = "Merchant",
                        StatusCode = "InProgress",
                        StatusDate = DateTime.Now,
                        StatusName = "",
                        Submitted = DateTime.Now,
                        Tags = new List<string> { "Tag1" },
                        TenantId = "Tenant1"
                    }
            };
        }

        private static ITenantTime GetTenantTime()
        {
            return new UtcTenantTime();
        }

        public async Task<bool> CheckIfDuplicateExists(UniqueParameterTypes parameterType, string parameterValue)
        {
            return await FakeFilterViewRepository.GetUsingUniqueAttributes(Abstractions.UniqueParameterTypes.Email, "abc@gmail.com")  != null;
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return FakeFilterViewRepository.GetAll();
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            return FakeFilterViewRepository.GetAllBySource(sourceType, sourceId);
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications()
        {
            return await FakeFilterViewRepository.GetAllExpiredApplications(DateTimeOffset.Now.AddDays(10));
        }

        public async Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return await FakeFilterViewRepository.GetByApplicationNumber(sourceType, sourceId, applicationNumber);
        }

        public async Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            return await FakeFilterViewRepository.GetByName(sourceType, sourceId, name);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return FakeFilterViewRepository.GetBySourceAndStatus(sourceType, sourceId, statuses);
        }

        public async Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return await FakeFilterViewRepository.GetByStatus(sourceType, sourceId, statuses);
        }

        public async Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return await FakeFilterViewRepository.GetByTag(sourceType, sourceId, tags);
        }

        public async Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return await FakeFilterViewRepository.GetCountByStatus(sourceType, sourceId, statuses);
        }

        public async Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        {
            return await FakeFilterViewRepository.GetCountByTag(sourceType, sourceId, tags);
        }

        public double GetTotalAmountApprovedBySource(string sourceType, string sourceId)
        {
            return FakeFilterViewRepository.GetTotalAmountApprovedBySourceAndStatus(sourceType, sourceId, new string[] { "InProgress" });
        }

        public async Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId)
        {
            return await FakeFilterViewRepository.GetTotalSubmittedForCurrentMonth(sourceType, sourceId, DateTimeOffset.Now);
            
        }

        public async Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId)
        {
            return await FakeFilterViewRepository.GetTotalSubmittedForCurrentYear(sourceType, sourceId, DateTimeOffset.Now);
        }

        public async Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            return await FakeFilterViewRepository.GetAllByStatus(statuses);
        }

        public void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public Task<IFilterView> GetByApplicationNumberWithStatusHistory(string sourceType, string sourceId, string applicationNumber)
        {
            throw new NotImplementedException();
        }
    }
}
