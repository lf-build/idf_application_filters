#!/usr/bin/env bash

# set the docker image name
IMAGE_NAME=""
if [ -n "$1" ]
then
  IMAGE_NAME="$1"
else
  echo "You need to specify the name of image. Example: ./deploy.sh myservice"
  exit 1
fi

# build the current project
docker build -t localhost:8118/$IMAGE_NAME . #:dev-$BUILD_NUMBER .

# publish the image to the registry
docker push localhost:8118/$IMAGE_NAME #:dev-$BUILD_NUMBER
